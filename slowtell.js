
/*
 *  Called when page is slower than set timeout
 *  The onbeforeunload event works in FF, IE and Safari, but not in Opera (yet).
 *  The onload event works in all browsers (not working in IE 7 not sure about 6.)
 *  
 */
 
$(document).ready(function() {	
  $('body').append($('.jqmWindow'));
  $('#slowtell_block').jqm({modal:Drupal.slowtell.modal, overlay:Drupal.slowtell.opaque});
	if (Drupal.slowtell.type == 'login') { //only  for user login form submissions
    $('#user-login-form').submit(initon);
  } else {
    onbeforeunload = initon;
  }

});
function initon() {   
  // this is not the best method... but still we don't have any other choice halt operation for a while
  var currentTime = new Date();
  var limit = currentTime.getTime() + Drupal.slowtell.delay*1000;
  do {
    currentTime = new Date();
  }while(currentTime.getTime() < limit);
  $('#slowtell_block').jqmShow();
}








